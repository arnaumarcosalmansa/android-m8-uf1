package com.example.ies.vistaslistas;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SecondaryActivity extends AppCompatActivity
{
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.secondary_activity);

        Button btn = (Button) findViewById(R.id.return_button);
        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                EditText text = (EditText) findViewById(R.id.editText);
                Intent intent = new Intent();
                Bundle data = new Bundle();

                data.putString("SUBTITULO", text.getText().toString());

                intent.putExtras(data);

                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}
