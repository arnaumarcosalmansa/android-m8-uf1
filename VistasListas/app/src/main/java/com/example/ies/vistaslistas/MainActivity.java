package com.example.ies.vistaslistas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    private Option[] options = new Option[10];
    private OptionArrayAdapter oaa;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        for(int i = 0; i < options.length; i++)
        {
            options[i] = new Option("Opción " + i, "Subtitulo " + i, i);
        }

        ListView lv = (ListView) findViewById(R.id.list_view);

        oaa = new OptionArrayAdapter(this, options);

        lv.setAdapter(oaa);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent intent = new Intent(getApplicationContext(), SecondaryActivity.class);
                startActivityForResult(intent, position);
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode == RESULT_OK)
        {
            Bundle bundle = data.getExtras();
            options[requestCode].setSubTitle(bundle.getString("SUBTITULO"));
            oaa.notifyDataSetChanged();
        }
    }
}
