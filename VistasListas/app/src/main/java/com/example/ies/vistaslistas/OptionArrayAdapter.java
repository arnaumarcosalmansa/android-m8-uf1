package com.example.ies.vistaslistas;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class OptionArrayAdapter extends ArrayAdapter
{
    Activity context;
    Option[] options;
    public OptionArrayAdapter(Activity context, Option[] options) {
        super(context, R.layout.option_layout, options);
        this.context = context;
        this.options = options;
    }


    // GetView s'executa per cada element de l'array de dades i el que fa
    // es "inflar" el layout del XML que hem creat

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = context.getLayoutInflater();
        View item = inflater.inflate(R.layout.option_layout, null);
        TextView lblTitulo = (TextView) item.findViewById(R.id.option_title);
        lblTitulo.setText(options[position].getTitle());
        TextView lblSubTitulo = (TextView) item.findViewById(R.id.option_subtitle);
        lblSubTitulo.setText(options[position].getSubTitle());
        return item;
    }

}
