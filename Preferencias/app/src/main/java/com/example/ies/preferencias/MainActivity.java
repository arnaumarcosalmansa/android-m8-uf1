package com.example.ies.preferencias;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.drawable.GradientDrawable;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{
    private TextView timesRotatedText;
    private TextView timesStartedText;
    private int lastRotation;
    private boolean isChecked = false;
    private int timesStarted;
    private int timesRotated;

    boolean firstStart;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            timesStartedText = (TextView) findViewById(R.id.startedTimesText);
            timesRotatedText = (TextView) findViewById(R.id.rotatedTimesText);

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

            firstStart = prefs.getBoolean("FIRST_START", true);
            timesStarted = prefs.getInt("TIMES_STARTED", 0);
            timesRotated = prefs.getInt("TIMES_ROTATED", 0);

            if (!firstStart)
            {
                timesStarted++;
            }
            else
            {
                timesRotated++;
            }

            timesStartedText.setText("INICIADO: " + timesStarted);
            timesRotatedText.setText("ROTADO: " + timesRotated);

            savePrefs(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        savePrefs(false);
    }

    protected void onPause()
    {
        super.onPause();
        if(firstStart)
        {
            savePrefs(false);
        }
        else {
            savePrefs(true);
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        savePrefs(true);
    }

    private void savePrefs(boolean isEnd)
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editorpreferencies = prefs.edit();
        editorpreferencies.putInt("TIMES_ROTATED", timesRotated);
        editorpreferencies.putInt("TIMES_STARTED", timesStarted);

        editorpreferencies.putBoolean("FIRST_START", isEnd);

        editorpreferencies.commit();
    }
}
