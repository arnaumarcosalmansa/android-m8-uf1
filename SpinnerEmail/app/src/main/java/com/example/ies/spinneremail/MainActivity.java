package com.example.ies.spinneremail;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity
{
    HashMap<String, EditText> editableTexts = new HashMap<String, EditText>();
    ArrayList<View> toggleViews = new ArrayList<View>();
    Button sendButton;
    String[] spinnerEntries = new String[] {"Muy Urgente","Urgente","Normal"};

    TextWatcher checkAllFilled = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count)
        {

        }

        @Override
        public void afterTextChanged(Editable s)
        {
            boolean isActive = true;
            for (Map.Entry<String, EditText> t : editableTexts.entrySet())
            {
                if(t.getValue().getText().toString().isEmpty())
                {
                    isActive = false;
                    break;
                }
            }

            for(View v : toggleViews)
            {
                v.setEnabled(isActive);
            }
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Enlazamos el sprinner al array declarado en tiempo de ejecucion
        Spinner sendPrioritySpinner = (Spinner)findViewById(R.id.spinner_priority);
        ArrayAdapter<String> spinnerOptions= new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spinnerEntries );
        sendPrioritySpinner.setAdapter(spinnerOptions);


        editableTexts.put("name", (EditText) findViewById(R.id.editText_name));
        editableTexts.put(android.content.Intent.EXTRA_EMAIL, (EditText) findViewById(R.id.editText_email));
        editableTexts.put(android.content.Intent.EXTRA_TEXT, (EditText) findViewById(R.id.editText_content));

        for (Map.Entry<String, EditText> entry : editableTexts.entrySet())
        {
            entry.getValue().addTextChangedListener(checkAllFilled);
        }

        Button sendButton;
        toggleViews.add(findViewById(R.id.checkBox));
        toggleViews.add(sendButton = findViewById(R.id.button_send));

        for(View v : toggleViews)
        {
            v.setEnabled(false);
        }

        sendButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Spinner tipoConsulta = (Spinner) findViewById(R.id.spinner_priority);
                String tipo = tipoConsulta.getSelectedItem().toString();

                CheckBox checkboxUsarCorreo = (CheckBox) findViewById(R.id.checkBox);
                boolean wantCC = checkboxUsarCorreo.isChecked();

                Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                emailIntent.setType("plain/text");

                for(Map.Entry<String, EditText> entry : editableTexts.entrySet())
                {
                    if(entry.equals(Intent.EXTRA_CC))
                    {
                        if(wantCC)
                        {
                            emailIntent.putExtra(entry.getKey(), entry.getValue().getText().toString());
                        }
                    }
                    else
                    {
                        emailIntent.putExtra(entry.getKey(), entry.getValue().getText().toString());
                    }
                }

                /** Enviaremos un eMail a travŽs de una Activity de la aplicaci—n de correo, con un Intent*/


                /** Usar el eMail de destino*/
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"milbicis@gmail.com"});

                /** Estableceremos el asunto*/
                String emailAsunto = "Comentarios Formulario";
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, emailAsunto);

                /** No a–adiremos adjuntos. Pero se indica la forma de hacerlo*/
                //emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);

                startActivity(Intent.createChooser(emailIntent, "Enviar correo..."));
                // CreateChooser fa que si hi ha més d’una aplicació per enviar correu, es pugi escollir quina fer servir.



            }
        });
    }






/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
*/
}
