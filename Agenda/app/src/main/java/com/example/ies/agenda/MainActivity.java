package com.example.ies.agenda;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
{
    private ArrayList<EditText> allEditTexts = new ArrayList<EditText>();
    private boolean locked = false;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LinearLayout root = (LinearLayout) findViewById(R.id.rootLinearLayout);

        for (int i = 0; i < root.getChildCount(); i++)
        {
            View v = root.getChildAt(i);
            if (v instanceof  EditText)
            {
                allEditTexts.add((EditText) v);
            }
        }

        Button button = (Button) findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                boolean allFilled = true;
                Button button = (Button) view;

                if (locked)
                {
                    locked = !locked;
                    for (EditText et : allEditTexts)
                    {
                        et.setEnabled(true);
                    }
                    button.setText(getString(R.string.button_lock));
                    return;
                }

                for (EditText et : allEditTexts)
                {
                    if(et.getText().toString().length() == 0)
                    {
                        allFilled = false;
                        break;
                    }
                }

                TextView tv = (TextView) findViewById(R.id.message);

                if (!allFilled)
                {
                    tv.setText("No todos los campos están llenos");
                    button.setText(getString(R.string.button_lock));
                }
                else
                {
                    tv.setText("Todos los campos llenos.");

                    locked = true;

                    button.setText(getString(R.string.button_unlock));
                    for (EditText et : allEditTexts)
                    {
                        et.setEnabled(false);
                    }
                }
            }
        });
    }
}
