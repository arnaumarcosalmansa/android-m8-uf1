package com.example.ies.calculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private String tag = "Punt del Cicle de Vida";
    private char[] numbers = {'1','2','3','4','5','6','7','8','9','0'};
    private char[] operators = {'+','-','*','/'};
    private char point = '.';
    private char lastChar = '\0';
    private char currentOperation = '\0';
    private char equals = '=';
    private char retrieve = 'r';

    private double operand1, operand2, memory;

    private TextView screen;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.screen = findViewById(R.id.screen);

        ArrayList<Button> buttons = new ArrayList<>();

        buttons.add((Button) findViewById(R.id.button_0));
        buttons.add((Button) findViewById(R.id.button_1));
        buttons.add((Button) findViewById(R.id.button_2));
        buttons.add((Button) findViewById(R.id.button_3));
        buttons.add((Button) findViewById(R.id.button_4));
        buttons.add((Button) findViewById(R.id.button_5));
        buttons.add((Button) findViewById(R.id.button_6));
        buttons.add((Button) findViewById(R.id.button_7));
        buttons.add((Button) findViewById(R.id.button_8));
        buttons.add((Button) findViewById(R.id.button_9));

        for(Button b : buttons)
        {
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    Button current = (Button) view;

                    char currentChar = current.getText().charAt(0);

                    if (lastChar == '=' || lastChar == 'r' || arrayContains(operators, lastChar))
                    {
                        screen.setText("" + currentChar);
                    }
                    else
                    {
                        if(screen.getText().toString().matches("0*"))
                        {
                            screen.setText("" + currentChar);
                        }
                        else
                        {
                            screen.append("" + currentChar);
                        }
                    }

                    lastChar = currentChar;
                }
            });
        }

        buttons.clear();

        buttons.add((Button) findViewById(R.id.button_add));
        buttons.add((Button) findViewById(R.id.button_sub));
        buttons.add((Button) findViewById(R.id.button_mul));
        buttons.add((Button) findViewById(R.id.button_div));

        for (Button b : buttons)
        {
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    Button current = (Button) view;

                    char currentChar = current.getText().charAt(0);

                    //SI EL ULTIMO CARACTER ES UN PUNTO NO PUEDE PARSEAR FLOAT
                    if(lastCharIsPoint()) return;

                    //SI NO HAY OPERCAION POR REALIZAR
                    //Y EL ULTIMO BOTON PULSADO FUE UN NUMERO O MR
                    if(currentOperation == '\0'  && ( arrayContains(numbers, lastChar) || lastChar == 'r' ) )
                    {
                        //COJE EL CONTENIDO DE LA PANTALLA Y LO METE EN operand1
                        operand1 = Double.parseDouble(screen.getText().toString());

                        //VACIA LA PANTALLA
                        screen.setText("");
                    }       //SI HAY UNA OPERACION POR HACER Y ESTA OPERACION NO FUE EL ULTIMO BOTON PULSADO (HAY UN NUMERO DESPUÉS)
                    else if (currentOperation != '\0' && currentOperation != lastChar)
                    {
                        //LEE LA PANTALLA Y ESCRIBE EL VALOR EN operand2
                        operand2 = Double.parseDouble(screen.getText().toString());

                        //HACE LA OPERACION
                        performCurrentOperation();

                        //PONE operand1 (EL RESULTADO) EN PANTALLA
                        showNumber(operand1);
                    }

                    //REGISTRA QUE EL ULTIMO BOTON PULSADO ES ESTE
                    //Y QUE LA OPERACION A REALIZAR ES LA DE ESTE BOTÓN
                    lastChar = currentChar;
                    currentOperation = currentChar;
                }
            });
        }

        Button point = (Button) findViewById(R.id.button_point);
        point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Button current = (Button) view;

                char currentChar = current.getText().charAt(0);

                //SI LA PANTALLA TIENE UN PUNTO YA PARA
                if(screenHasDecimal()) return;

                //SI EL ULTIMO BOTON PULSADO FUE UN NUMERO
                if (arrayContains(numbers,lastChar))
                {
                    //PONE UN PUNTO
                    screen.append("" + currentChar);
                    //REGISTRA QUE ESTE FUE EL ULTIMO CARACTER
                    lastChar = currentChar;
                }       //SI EL ULTIMO BOTON PULSADO FUE UNA OPERACION
                else if(arrayContains(operators, lastChar) || lastChar == '\0')
                {
                    //PONE UN "0" Y UN PUNTO
                    screen.append("0" + currentChar);
                    //REGISTRA QUE ESTE FUE EL ULTIMO CARACTER
                    lastChar = currentChar;
                }
            }
        });

        Button equals = (Button) findViewById(R.id.button_equals);

        equals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if (arrayContains(numbers, lastChar) && currentOperation != '\0')
                {
                    operand2 = Double.parseDouble(screen.getText().toString());

                    performCurrentOperation();

                    showNumber(operand1);

                    currentOperation = '\0';
                    lastChar = '=';
                }

            }
        });

        Button mc = (Button) findViewById(R.id.button_mc);
        mc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                memory = 0f;
            }
        });

        Button mr = (Button) findViewById(R.id.button_mr);
        mr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
               showNumber(memory);
               lastChar = 'r';
            }
        });

        Button mplus = (Button) findViewById(R.id.button_mplus);
        mplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                String num = screen.getText().toString();
                if(!num.equals(""))
                {
                    memory += Double.parseDouble(num);
                }
            }
        });

        Button mminus = (Button) findViewById(R.id.button_mminus);
        mminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                String num = screen.getText().toString();
                if(!num.equals(""))
                {
                    memory -= Double.parseDouble(num);
                }
            }
        });

        Button buttonC = (Button) findViewById(R.id.button_c);

        buttonC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                lastChar = '\0';
                currentOperation = '\0';

                operand1 = 0;
                operand2 = 0;
                memory = 0;

                screen.setText("");
            }
        });

        Log.d(tag, "onCreate()");
    }

    private boolean arrayContains(char[] array, char c)
    {
        for (char a : array)
        {
            if(a == c) return true;
        }

        return false;
    }

    private void performCurrentOperation()
    {
        switch (currentOperation)
        {
            case '+':
                operand1 = operand1 + operand2;
                break;
            case '-':
                operand1 = operand1 - operand2;
                break;
            case '*':
                operand1 = operand1 * operand2;
                break;
            case '/':
                operand1 = operand1 / operand2;
                break;
            default:
                System.out.println("aaaaaaaaaa");
                break;
        }
    }

    private boolean screenHasDecimal()
    {
        return screen.getText().toString().contains(".");
    }

    private boolean lastCharIsPoint()
    {
        String s = screen.getText().toString();
        return s.charAt(s.length() - 1) == '.';
    }

    private void showNumber(double number)
    {
        screen.setText(new Double(number).toString().replaceAll("\\.?0*$", ""));
    }

    protected void onStart()
    {
        super.onStart();
        Log.d(tag, "onStart()");
    }

    protected void onRestart()
    {
        super.onRestart();
        Log.d(tag, "onRestart()");
    }

    protected void onResume()
    {
        super.onResume();
        Log.d(tag, "onRestart()");
    }

    protected void onPause()
    {
        super.onPause();
        Log.d(tag, "onPause()");
    }

    protected void onStop()
    {
        super.onStop();
        Log.d(tag, "onStop()");
    }

    protected void onDestroy()
    {
        super.onDestroy();
        Log.d(tag, "onDestroy()");
    }
}
