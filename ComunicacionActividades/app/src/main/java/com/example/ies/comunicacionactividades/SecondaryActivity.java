package com.example.ies.comunicacionactividades;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SecondaryActivity extends AppCompatActivity implements View.OnClickListener
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
	{
        super.onCreate(savedInstanceState);
        setContentView(R.layout.secondary_activity);
		
		Bundle data = this.getIntent().getExtras();
		
		TextView text = (TextView) findViewById(R.id.text_sortida);
		
		text.setText("Hola, " + data.getString("nombre") + "\n¿Aceptas las condiciones?");

		Button aceptar = (Button) findViewById(R.id.button_accept);
		aceptar.setOnClickListener(this);
        Button rechazar = (Button) findViewById(R.id.button_decline);
        rechazar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        Intent inData = new Intent();
        boolean kill = false;
        switch (v.getId())
        {
            case R.id.button_accept:
                inData.putExtra("RESULTADO", "Resultado Correcto");
                kill = true;
                break;
            case R.id.button_decline:
                inData.putExtra("RESULTADO", "Resultado Erróneo");
                kill = true;
                break;
            default:
                break;
        }
        if(kill)
        {
            setResult(Activity.RESULT_OK, inData);
            finish();
        }

    }

}
