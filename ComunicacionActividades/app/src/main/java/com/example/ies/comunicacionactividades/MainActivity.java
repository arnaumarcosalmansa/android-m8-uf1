package com.example.ies.comunicacionactividades;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
	{
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
		
		Button verificar = (Button) findViewById(R.id.button_verificar);
		
		verificar.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view)
			{
				EditText text = (EditText) findViewById(R.id.edittext_nom);
				
				Intent intent = new Intent(getApplicationContext(), SecondaryActivity.class);
				
				Bundle data = new Bundle();
				
				data.putString("nombre", text.getText().toString());
				
				intent.putExtras(data);
				
				startActivityForResult(intent, 1);
			}
		});
    }

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		TextView result = (TextView) findViewById(R.id.text_result);
		if (requestCode == 1)
		{
			if (resultCode == RESULT_OK)
			{
				result.setText((String) data.getExtras().get("RESULTADO"));
			}
			else
			{
				result.setText("Error.");
			}
		}
		else
		{
			result.setText("Activity no controlada.");
		}
	}
}
