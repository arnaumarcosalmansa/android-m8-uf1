package com.example.ies.muchasactivitiesnumeradas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private long activityId = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView textView = (TextView) findViewById(R.id.text_results);
        textView.setMovementMethod(new ScrollingMovementMethod());

        Button crear = (Button) findViewById(R.id.button_create);

        crear.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                activityId++;

                Intent intent = new Intent(getApplicationContext(), NumeratedActivity.class);

                Bundle data = new Bundle();

                data.putLong("ID", (Long) activityId);


                intent.putExtras(data);

                try
                {
                    startActivityForResult(intent, 1);
                }
                catch(Exception e)
                {
                    Log.d("mensaje " + e.getLocalizedMessage(), e.getMessage());
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {

        super.onActivityResult(requestCode, resultCode, data);
        TextView result = (TextView) findViewById(R.id.text_results);

        if (requestCode == 1)
        {
            String mensaje = data.getExtras().getLong("ID") + ": " + data.getExtras().getString("MESSAGE") + "\n";

            if (resultCode == RESULT_OK)
            {
                result.append(mensaje);
            }
            else
            {
                //result.setText("Error.");
            }
        }
        else
        {
            result.append("Activity no controlada.");
        }
    }
}
