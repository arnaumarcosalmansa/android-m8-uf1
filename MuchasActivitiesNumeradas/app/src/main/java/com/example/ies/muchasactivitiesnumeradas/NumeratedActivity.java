package com.example.ies.muchasactivitiesnumeradas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class NumeratedActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numerated);

        Button volver = (Button) findViewById(R.id.button_return);
        volver.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                EditText message = (EditText) findViewById(R.id.message_text);
                Intent intent = new Intent();
                Bundle data = new Bundle();
                data.putLong("ID", (Long) NumeratedActivity.super.getIntent().getExtras().get("ID"));
                data.putString("MESSAGE", message.getText().toString());

                intent.putExtras(data);

                setResult(Activity.RESULT_OK, intent);

                finish();
            }
        });
    }
}
