package com.example.ies.holamoncode;

import android.graphics.Color;
import android.graphics.Point;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{

    private TextView tv;
    private TextView numTv;
    private LinearLayout vll;
    private LinearLayout vll2;
    private LinearLayout hll;
    private LinearLayout hll2;
    private Button hideButton;
    private Button showButton;


    private int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        ///setContentView(R.layout.activity_main);

        Point size = new Point();

        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(size);

        tv = new TextView(getApplicationContext());
        tv.setText(R.string.content);
        tv.setTextColor(Color.rgb(0,0,0));
        tv.setTextSize(20);

        numTv = new TextView(getApplicationContext());
        numTv.setText(String.valueOf(counter));
        numTv.setTextColor(Color.rgb(0,0,0));
        numTv.setTextSize(20);
        numTv.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(size.x, size.y);

        vll = new LinearLayout(getApplicationContext());
        vll.setOrientation(LinearLayout.VERTICAL);
        vll.setLayoutParams(lp);
        vll.addView(tv);
        vll.addView(numTv);

        vll2 = new LinearLayout((getApplicationContext()));
        vll2.setOrientation(LinearLayout.VERTICAL);
        vll2.setLayoutParams(new LinearLayout.LayoutParams(size.x, size.y / 3));
        vll.addView(vll2);

        hll = new LinearLayout(getApplicationContext());
        hll.setOrientation(LinearLayout.HORIZONTAL);
        hll.setLayoutParams(new LinearLayout.LayoutParams(size.x, size.y * 2 / 3));
        hll.setGravity(Gravity.CENTER_HORIZONTAL);
        vll2.addView(hll);

        hll2 = new LinearLayout(getApplicationContext());
        hll2.setOrientation(LinearLayout.HORIZONTAL);
        hll2.setLayoutParams(new LinearLayout.LayoutParams(size.x, size.y / 3));
        hll2.setGravity(Gravity.CENTER_HORIZONTAL);

        vll2.addView(hll2);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        params.setMargins(10,10,10,10);


        hideButton = new Button(getApplicationContext());
        hideButton.setText("HIDE");
        hideButton.setBackgroundColor(Color.rgb(150,150,150));
        hideButton.setWidth(size.x / 2 - 40);
        //hideButton.setGravity(Gravity.CENTER);
        hideButton.setLayoutParams(params);



        showButton = new Button(getApplicationContext());
        showButton.setText("SHOW");
        showButton.setBackgroundColor(Color.rgb(150,150,150));
        showButton.setWidth(size.x / 2 - 40);
        //showButton.setGravity(Gravity.CENTER);
        showButton.setLayoutParams(params);



        tv.setWidth(size.x);
        tv.setHeight(size.y * 2 / 3);

        showButton.setEnabled(false);

        hll.addView(hideButton);
        hll.addView(showButton);

        hideButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                tv.setVisibility(TextView.INVISIBLE);

                showButton.setEnabled(true);
                hideButton.setEnabled(false);
            }
        });

        showButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                tv.setVisibility(TextView.VISIBLE);

                showButton.setEnabled(false);
                hideButton.setEnabled(true);
            }
        });


        setContentView(vll);

    }
}
