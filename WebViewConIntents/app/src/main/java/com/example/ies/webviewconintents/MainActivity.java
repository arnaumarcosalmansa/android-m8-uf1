package com.example.ies.webviewconintents;

import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity
{
    private Button buttonWebView, buttonBrowser, buttonCall, buttonMap, buttonContact;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonWebView = (Button) findViewById(R.id.button_webView);
        buttonWebView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getApplicationContext(), WebViewActivity.class);
                startActivity(intent);
            }
        });

        buttonBrowser = (Button) findViewById(R.id.button_browser);
        buttonBrowser.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://agora.xtec.cat/ies-sabadell/"));
                startActivity(intent);
            }
        });

        buttonCall = (Button) findViewById(R.id.button_call);
        buttonCall.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:+612345678"));
                startActivity(intent);
            }
        });

        buttonMap = (Button) findViewById(R.id.button_map);
        buttonMap.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:84.47159, 143.14429"));

                startActivity(intent);
            }
        });

        buttonContact = (Button) findViewById(R.id.button_contact);
        buttonContact.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(android.content.Intent.ACTION_PICK);
                intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
                startActivityForResult(intent,1);
            }
        });
    }
}
