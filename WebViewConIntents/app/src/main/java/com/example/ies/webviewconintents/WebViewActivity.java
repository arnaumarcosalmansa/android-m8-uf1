package com.example.ies.webviewconintents;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class WebViewActivity extends AppCompatActivity
{
    private WebView webView;
    private Activity self = this;
    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        Log.d("tag 1", "Altura: " + webView.getHeight() + " Anchura: " + webView.getWidth());

        webView = (WebView) findViewById(R.id.web_view);
        webView.setBackgroundColor(0);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.setInitialScale(50);

        webView.setWebViewClient(new WebViewClient());

        webView.loadUrl("https://www.google.com/");

        setContentView(webView);

        Log.d("tag 2", "Altura: " + webView.getHeight() + " Anchura: " + webView.getWidth());
    }
}
