package com.example.ies.contarletras;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //getWindow().setBackgroundDrawable(R.drawable.btn_dialog);
        getWindow().setTitle(getString(R.string.new_app_name));
        LinearLayout ll = (LinearLayout) findViewById(R.id.linearLayout);
        ll.setBackgroundColor(Color.rgb(255, 255, 225));

        tv = (TextView) findViewById(R.id.textView);

        EditText editText = (EditText) findViewById(R.id.editText);

        editText.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                int length = editable.toString().length();

                if(length <= 0)
                {
                    tv.setText(getString(R.string.default_text));
                }
                else
                {
                    tv.setText(getString(R.string.characters) + length);
                }
            }
        });
    }
}
