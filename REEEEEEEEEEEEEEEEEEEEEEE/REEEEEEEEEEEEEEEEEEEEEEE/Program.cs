using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REEEEEEEEEEEEEEEEEEEEEEE
{
    class Program
    {
        static void Main(string[] args)
        {
            // The code provided will print ‘Hello World’ to the console.
            // Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.
            Console.WriteLine("Hello World!");
            //Console.ReadKey();

            // Go to http://aka.ms/dotnet-get-started-console to continue learning how to build a console app!

            var palabras = new string[] { "patata", "arbol", "rueda" };
            var conA = from palabra in palabras where palabra.Contains("a" +
                       "") select palabra;

            foreach(var palabra in conA)
            {
                Console.WriteLine(palabra);
            }

            Queue<string> cola = new Queue<string>();

            cola.Enqueue("hola");
            cola.Dequeue();

            Stack<string> pila = new Stack<string>();

            pila.Push("adios");
            pila.Peek();
            pila.Pop();
        }
    }
}
