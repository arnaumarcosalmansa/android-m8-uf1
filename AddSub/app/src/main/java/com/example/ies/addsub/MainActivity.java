package com.example.ies.addsub;

import android.graphics.Color;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{
    private TextView numTv;
    private static int counter = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        Point size = new Point();

        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(size);


        numTv = new TextView(getApplicationContext());
        numTv.setText(String.valueOf(counter));
        numTv.setTextColor(Color.rgb(0,0,0));
        numTv.setTextSize(20);
        numTv.setGravity(Gravity.CENTER);


        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(size.x, size.y);

        LinearLayout vll = new LinearLayout(getApplicationContext());
        vll.setOrientation(LinearLayout.VERTICAL);
        vll.setLayoutParams(lp);
        vll.addView(numTv);


        LinearLayout hll = new LinearLayout((getApplicationContext()));
        hll.setOrientation(LinearLayout.HORIZONTAL);
        hll.setLayoutParams(new LinearLayout.LayoutParams(size.x, size.y / 3));

        Button add = new Button(getApplicationContext());
        add.setText("+");

        Button sub = new Button(getApplicationContext());
        sub.setText("-");

        hll.addView(sub);
        hll.addView(add);

        vll.addView(hll);

        setContentView(vll);


        add.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                counter++;
                updateNum();
            }
        });

        sub.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                counter--;
                updateNum();
            }
        });

    }

    private void updateNum()
    {
        numTv.setText(String.valueOf(counter));
    }
}
